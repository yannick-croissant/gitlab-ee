## GitLab
##

## Redirects all HTTP traffic to the HTTPS host
server {
  ## Either remove "default_server" from the listen line below,
  ## or delete the /etc/nginx/sites-enabled/default file. This will cause gitlab
  ## to be served if you visit any address that your server responds to, eg.
  ## the ip address of the server (http://x.x.x.x/)
  listen 0.0.0.0:80;
  listen [::]:80 ipv6only=on;

  ## Replace this with something like pages.gitlab.com
  server_name ~^(?<group>.*)\.YOUR_GITLAB_PAGES\.DOMAIN$;
  server_tokens off; ## Don't show the nginx version number, a security best practice

  return 301  https://$http_host$request_uri;

  access_log  /var/log/nginx/gitlab_pages_access.log;
  error_log   /var/log/nginx/gitlab_pages_access.log;
}

## Pages serving host
server {
  listen 0.0.0.0:443 ssl;
  listen [::]:443 ipv6only=on ssl;

  ## Replace this with something like pages.gitlab.com
  server_name ~^(?<group>.*)\.YOUR_GITLAB_PAGES\.DOMAIN$;
  server_tokens off; ## Don't show the nginx version number, a security best practice
  root /home/git/gitlab/shared/pages/${group};

  ## Strong SSL Security
  ## https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html & https://cipherli.st/
  ssl on;
  ssl_certificate /etc/nginx/ssl/gitlab-pages.crt;
  ssl_certificate_key /etc/nginx/ssl/gitlab-pages.key;

  # GitLab needs backwards compatible ciphers to retain compatibility with Java IDEs
  ssl_ciphers "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_session_cache shared:SSL:10m;
  ssl_session_timeout 5m;

  ## See app/controllers/application_controller.rb for headers set

  ## [Optional] If your certficate has OCSP, enable OCSP stapling to reduce the overhead and latency of running SSL.
  ## Replace with your ssl_trusted_certificate. For more info see:
  ## - https://medium.com/devops-programming/4445f4862461
  ## - https://www.ruby-forum.com/topic/4419319
  ## - https://www.digitalocean.com/community/tutorials/how-to-configure-ocsp-stapling-on-apache-and-nginx
  # ssl_stapling on;
  # ssl_stapling_verify on;
  # ssl_trusted_certificate /etc/nginx/ssl/stapling.trusted.crt;

  ## [Optional] Generate a stronger DHE parameter:
  ##   sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 4096
  ##
  # ssl_dhparam /etc/ssl/certs/dhparam.pem;

  ## Individual nginx logs for GitLab pages
  access_log  /var/log/nginx/gitlab_pages_access.log;
  error_log   /var/log/nginx/gitlab_pages_error.log;

  # 1. Try to get /path/ from shared/pages/${group}/${path}/public/
  # 2. Try to get / from shared/pages/${group}/${host}/public/
  location ~ ^/([^/]*)(/.*)?$ {
    try_files "/$1/public$2"
              "/$1/public$2/index.html"
              "/${host}/public/${uri}"
              "/${host}/public/${uri}/index.html"
              =404;
  }

  # Define custom error pages
  error_page 403 /403.html;
  error_page 404 /404.html;
}
